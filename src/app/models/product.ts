export class Product {
    $key: string;
    name: string;
    description: string;
    date: number;
}
