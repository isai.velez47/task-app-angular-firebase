import { Injectable } from '@angular/core';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

// Model
import { Product } from '../models/product';

@Injectable()
export class ProductService {

  productList: AngularFireList<any>;
  selectedProduct: Product = new Product();
  abrir: boolean = false;

  constructor(private firebase: AngularFireDatabase) { }

  abrirModal() {
    return this.abrir = true;
  }

  cerrarModal() {
    return this.abrir = false;
  }

  getProducts()
  {
    return this.productList = this.firebase.list('products');
  }

  insertProduct(product: Product)
  {
    this.productList.push({
      name: product.name,
      description: product.description,
      date: product.date
    });
  }

  updateProduct(product: Product)
  {
    this.productList.update(product.$key, {
      name: product.name,
      description: product.description,
      date: product.date
    });
  }

  deleteProduct($key: string)
  {
    this.productList.remove($key);
  }
}
