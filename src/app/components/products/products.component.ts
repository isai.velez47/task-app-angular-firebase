import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  pendingScreen: boolean;
  addTask: boolean;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.pendingScreen = true;
    this.addTask = false;
  }

  toPendingScreen() {
    this.pendingScreen = true;
  }

  toFinishedScreen() {
    this.pendingScreen = false;
  }

  clickAddTask() {
    this.productService.abrirModal();
    this.addTask = this.productService.abrir;
  }

  clickCloseModal() {
    this.addTask = false;
  }
}
