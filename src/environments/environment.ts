// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBuEHStjf-673Orhs7kovKk9LHYNMy09c0",
    authDomain: "taskapp-ea4f4.firebaseapp.com",
    databaseURL: "https://taskapp-ea4f4.firebaseio.com",
    projectId: "taskapp-ea4f4",
    storageBucket: "taskapp-ea4f4.appspot.com",
    messagingSenderId: "526119392851"
  }
};
